'use strict';

/**
 * @ngdoc overview
 * @name frontrockApp
 * @description
 * # frontrockApp
 *
 * Main module of the application.
 */
angular
  .module('frontrockApp', [
    'ngAnimate',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ng-token-auth',
  ])
  .config(function ($stateProvider, $authProvider) {
    $authProvider.configure({
      apiUrl: 'http://localhost:3000'
    });

    $stateProvider.state('routes', {
      url: '/routes',
      templateUrl: 'views/routes/index.html',
      controller: 'IndexRoutesCtrl',
      controllerAs: 'vm'
    }).state('newRoutes', {
      url: '/routes/new',
      templateUrl: 'views/routes/new.html',
      controller: 'NewRoutesCtrl',
      controllerAs: 'vm',
      resolve: {
        auth: function($auth) {
          return $auth.validateUser();
        }
      }
    }).state('showRoutes', {
      url: '/routes/:id',
      templateUrl: 'views/routes/show.html',
      controller: 'ShowRoutesCtrl',
      controllerAs: 'vm'
    }).state('editRoutes', {
      url: '/routes/:id/edit',
      templateUrl: 'views/routes/edit.html',
      controller: 'EditRoutesCtrl',
      controllerAs: 'vm',
      resolve: {
        auth: function($auth) {
          return $auth.validateUser();
        }
      }
    });
  }).run(function($state) {
    $state.go('routes');
  });
