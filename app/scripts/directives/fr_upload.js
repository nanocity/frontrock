'use strict';

angular.module('frontrockApp').
  directive('frUpload', function(){
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {
        callback: '&'
      },
      link: function link(scope, element, attrs, ctrl) {
        function onChange() {
          ctrl.$setViewValue(element[0].files[0]);

          if(angular.isDefined(attrs.callback)){
            scope.callback();
          }
        }

        element.on('change', onChange);

        scope.$on('destroy', function () {
          element.off('change', onChange);
        });
      }
    };
  });
