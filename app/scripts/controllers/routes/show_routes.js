'use strict';

angular.module('frontrockApp')
  .controller('ShowRoutesCtrl', function ($stateParams, Route) {
    var vm = this;
    vm.route = Route.get({ id: $stateParams.id });
  });
