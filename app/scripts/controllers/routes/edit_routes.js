'use strict';

angular.module('frontrockApp')
  .controller('EditRoutesCtrl', function ($state, $stateParams, Route) {
    var vm = this;
    vm.route = Route.get({ id: $stateParams.id });

    vm.addImage = function(){
      vm.route.images.push({attachment: null});
    };

    vm.update = function(){
      vm.route.$update().then(function(){
        $state.go('showRoutes', { id: vm.route.id });
      });
    };

    vm.destroy = function(){
      vm.route.$delete().then(function(){
        $state.go('routes');
      });
    };
  });
