'use strict';

angular.module('frontrockApp')
  .controller('NewRoutesCtrl', function (Route, $state) {
    var vm = this;
    vm.route = new Route();
    vm.route.images = [];

    vm.addImage = function(){
      vm.route.images.push({attachment: null});
    };

    vm.create = function(){
      vm.route.$save().then(function(){
        $state.go('routes');
      });
    };
  });
