'use strict';

angular.module('frontrockApp')
  .controller('IndexRoutesCtrl', function (Route) {
    var vm = this;
    vm.routes = Route.query();
  });
