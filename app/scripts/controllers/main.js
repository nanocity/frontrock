'use strict';

angular.module('frontrockApp')
  .controller('MainCtrl', function ($rootScope, $auth) {
    var vm = this;
    $auth.validateUser().then(function(user){
      vm.user = user;
    });

    $rootScope.$on('auth:oauth-registration', function(evt, user){ vm.user = user; });
    $rootScope.$on('auth:logout-success', function(){ vm.user = null; });
  });
