'use strict';

angular.module('frontrockApp').
  factory('Route', function($resource){
    return $resource(
      'http://localhost:3000/api/routes/:id.json', { id: '@id' },
      {
        save: {
          method: 'POST',
          headers: { 'Content-Type': undefined },
          transformRequest: function (data) {
            var formData = new FormData();
            formData.append('route[title]', data.title);
            formData.append('route[description]', data.description);

            for (var i = 0; i < data.images.length; i++) {
              formData.append('route[images_attributes]['+i+'][attachment]', data.images[i].attachment);
            }

            return formData;
          }
        },
        update: {
          method: 'PUT',
          headers: { 'Content-Type': undefined },
          transformRequest: function (data) {
            var formData = new FormData();
            formData.append('route[id]', data.id);
            formData.append('route[title]', data.title);
            formData.append('route[description]', data.description);

            for (var i = 0; i < data.images.length; i++) {
              formData.append('route[images_attributes]['+i+'][attachment]', data.images[i].attachment);


              if(!!(data.images[i].id)){
                formData.append('route[images_attributes]['+i+'][id]', data.images[i].id);
              }

              if(!!(data.images[i]._destroy)){
                formData.append('route[images_attributes]['+i+'][_destroy]', !!(data.images[i]._destroy));
              }
            }

            return formData;
          }
        }
      }
    );
  });
